INSERT INTO image (ID, TYPE, IMAGES, DELETED, CREATED, UPDATED) VALUES
(11, 'product', 'http://this.png,http://this.png', 0, NOW(), NOW()),
(22, 'product', 'http://that.png,http://that.png', 0, NOW(), NOW()),
(33, 'business', 'http://biz.png,http://ibz.png', 0, NOW(), NOW());

INSERT INTO business (ID, DISPLAY_NAME, NAME, ADDRESS, DESCRIPTION, EMAIL, PHONE, TYPE, ACTIVE, IMAGE_ICON, IMAGES_ID, DELETED, CREATED, UPDATED) VALUES
(11, 'Soundarya Bakery', 'Soundarya Bakery', '#123, Bakers Street', 'A Modern Bakery in Bangalore North', 'soundaryabakes@inventory.com', '9988888899', 'Bakery', 1, 'http://sound.com/image.png', 33, 0, NOW(), NOW()),
(22, 'Krishna Bakery', 'Krishna Bakery', '#124, Bakers Street', 'A Modern Bakery in Bangalore', 'krishnabakes@inventory.com', '9988888888', 'Bakery', 1, null, 33,0, NOW(), NOW()),
(33, 'Nagarjuna', 'Nagarjuna', '#123, Cook Street', 'A Modern Restaurant in Bangalore South', 'nagarjunacooks@inventory.com', '+919988888877', 'Restaurant', 1, 'http://nag.com/image.png', null,0, NOW(), NOW());

INSERT INTO category (ID, DISPLAY_NAME, NAME, DESCRIPTION, DELETED, CREATED, UPDATED) VALUES
(11, 'Regular', 'Regular', 'Regular savouries available all the time', 0, NOW(), NOW()),
(22, 'Special', 'Special', 'Special savouries available morning only', 0, NOW(), NOW()),
(33, 'Drinks', 'Drinks', 'Drinks available until stocks last', 0, NOW(), NOW());

INSERT INTO sub_category (ID, DISPLAY_NAME ,NAME , OFFER, DESCRIPTION, IMAGE_ICON, CATEGORY_ID, DELETED, CREATED, UPDATED) VALUES
(11, 'Puff', 'Puff', 'None', 'Awesome puffs that tastes like butter', 'http://puff.com/puff.png', 11, 0, NOW(), NOW()),
(22, 'Bread', 'Bread', '10%', 'Super breads of all varieties', null, 11, 0, NOW(), NOW()),
(33, 'Cool Drinks', 'Cool Drinks', 'None', 'Chill drinks to beat summer heat', null, 33, 0, NOW(), NOW());

INSERT INTO product (ID, DISPLAY_NAME, NAME, QUANTIFIER, COST, DESCRIPTION, DISCOUNT, TAX, PRICE, IMAGE_ICON, BUSINESS_ID, SUB_CATEGORY_ID, IMAGES_ID, DELETED, CREATED, UPDATED) VALUES
(11, 'Veg Puff', 'Veg Puff', '1pcs', 15, 'Awesome puffs that tastes like butter, made with cabbage and onions', 0, 0, 15, 'http://puff.com/veg_puff.png', 11, 11, 11, 0, NOW(), NOW()),
(22, 'Egg Puff', 'Egg Puff', '1pcs', 20, 'Speciality egg puffs', 5, 0, 15, null, 11, 11, 22, 0, NOW(), NOW()),
(33, 'Bread Basic', 'Bread Basic', '300gms', 20, 'Basic flour bread', 0, 2, 22, 'http://inventory.com/brean/basic.jpg', 33, 22, 11, 0, NOW(), NOW());
