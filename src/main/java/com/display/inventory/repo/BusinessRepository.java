package com.display.inventory.repo;

import com.display.inventory.entity.Business;
import org.springframework.data.repository.CrudRepository;

public interface BusinessRepository extends CrudRepository<Business, Long> {

}
