package com.display.inventory.repo;

import com.display.inventory.entity.Cart;
import org.springframework.data.repository.CrudRepository;

public interface CartRepository extends CrudRepository<Cart, Long> {


}
