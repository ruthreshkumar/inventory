package com.display.inventory.repo;

import com.display.inventory.entity.Content;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContentRepository extends CrudRepository<Content, Long> {

    List<Content> findByCartId(Long cartId);

}
