package com.display.inventory.repo;

import com.display.inventory.entity.SubCategory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {

    List<SubCategory> findByCategoryId(Long categoryId);

}
