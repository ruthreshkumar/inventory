package com.display.inventory.repo;

import com.display.inventory.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findBySubCategoryId(Long subCategoryId);

    List<Product> findByBusinessId(Long businessId);

}
