package com.display.inventory.controller;

import com.display.inventory.entity.*;
import com.display.inventory.services.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class InventoryController {

    private static final Logger logger = LogManager.getLogger(InventoryController.class);

    @Autowired
    private BusinessService businessService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SubCategoryService subCategoryService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ImageService imageService;

    @GetMapping(value ={"admin/business/v1", "business/v1"})
    public ResponseEntity getBusiness() {
        logger.info("::in getBusiness::");
        return ResponseEntity.ok(businessService.getBusiness());
    }

    @GetMapping(value ={"admin/business/{id}/v1", "business/{id}/v1"})
    public ResponseEntity getBusinessById(@PathVariable @NonNull Long id) {
        logger.info("::in getBusinessById::");
        return ResponseEntity.ok(businessService.getBusinessById(id));
    }

    @DeleteMapping("admin/business/{id}/v1")
    public ResponseEntity deleteBusinessById(@PathVariable @NonNull Long id) {
        logger.info("::in deleteBusinessById::");
        return ResponseEntity.ok(businessService.deleteBusinessById(id));
    }

    @PostMapping("admin/business/v1")
    public ResponseEntity createBusiness(@RequestBody Business business) {
        logger.info("::in createBusiness::");
        return ResponseEntity.ok(businessService.createOrUpdateBusiness(business));
    }

    @PutMapping("admin/business/v1")
    public ResponseEntity updateBusiness(@RequestBody Business business) {
        logger.info("::in updateBusiness::");
        return ResponseEntity.ok(businessService.createOrUpdateBusiness(business));
    }

    @GetMapping(value ={"admin/category/v1", "category/v1"})
    public ResponseEntity getCategory() {
        logger.info("::in getCategory::");
        return ResponseEntity.ok(categoryService.getCategory());
    }

    @GetMapping(value ={"admin/category/{id}/v1", "category/{id}/v1"})
    public ResponseEntity getCategoryById(@PathVariable @NonNull Long id) {
        logger.info("::in getCategoryById::");
        return ResponseEntity.ok(categoryService.getCategoryById(id));
    }

    @DeleteMapping("admin/category/{id}/v1")
    public ResponseEntity deleteCategoryById(@PathVariable @NonNull Long id) {
        logger.info("::in deleteCategoryById::");
        return ResponseEntity.ok(categoryService.deleteCategoryById(id));
    }

    @PostMapping("admin/category/v1")
    public ResponseEntity createCategory(@RequestBody Category category) {
        logger.info("::in createCategory::");
        return ResponseEntity.ok(categoryService.createOrUpdateCategory(category));
    }

    @PutMapping("admin/category/v1")
    public ResponseEntity updateCategory(@RequestBody Category category) {
        logger.info("::in updateCategory::");
        return ResponseEntity.ok(categoryService.createOrUpdateCategory(category));
    }

    @GetMapping(value ={"admin/sub_category/v1", "sub_category/v1"})
    public ResponseEntity getSubCategory() {
        logger.info("::in getSubCategory::");
        return ResponseEntity.ok(subCategoryService.getSubCategory());
    }

    @GetMapping(value ={"admin/sub_category/{id}/v1", "sub_category/{id}/v1"})
    public ResponseEntity getSubCategoryById(@PathVariable @NonNull Long id) {
        logger.info("::in getSubCategoryById::");
        return ResponseEntity.ok(subCategoryService.getSubCategoryById(id));
    }

    @DeleteMapping("admin/sub_category/{id}/v1")
    public ResponseEntity deleteSubCategoryById(@PathVariable @NonNull Long id) {
        logger.info("::in deleteSubCategoryById::");
        return ResponseEntity.ok(subCategoryService.deleteSubCategoryById(id));
    }

    @PostMapping("admin/sub_category/v1")
    public ResponseEntity createSubCategory(@RequestBody SubCategory subCategory) {
        logger.info("::in createSubCategory::");
        return ResponseEntity.ok(subCategoryService.createOrUpdateSubCategory(subCategory));
    }

    @PutMapping("admin/sub_category/v1")
    public ResponseEntity updateSubCategory(@RequestBody SubCategory subCategory) {
        logger.info("::in updateSubCategory::");
        return ResponseEntity.ok(subCategoryService.createOrUpdateSubCategory(subCategory));
    }

    @GetMapping(value ={"admin/product/v1", "product/v1"})
    public ResponseEntity getProduct() {
        logger.info("::in getProduct::");
        return ResponseEntity.ok(productService.getProducts());
    }

    @GetMapping(value ={"admin/product/{id}/v1", "product/{id}/v1"})
    public ResponseEntity getProductById(@PathVariable @NonNull Long id) {
        logger.info("::in getProductById::");
        return ResponseEntity.ok(productService.getProductById(id));
    }

    @DeleteMapping("admin/product/{id}/v1")
    public ResponseEntity deleteProductById(@PathVariable @NonNull Long id) {
        logger.info("::in deleteProductById::");
        return ResponseEntity.ok(productService.deleteProductById(id));
    }

    @PostMapping("admin/product/v1")
    public ResponseEntity createProduct(@RequestBody Product product) {
        logger.info("::in createProduct::");
        return ResponseEntity.ok(productService.createOrUpdateProduct(product));
    }

    @PutMapping("admin/product/v1")
    public ResponseEntity updateProduct(@RequestBody Product product) {
        logger.info("::in updateProduct::");
        return ResponseEntity.ok(productService.createOrUpdateProduct(product));
    }

    @GetMapping(value ={"admin/image/v1", "image/v1"})
    public ResponseEntity getImage() {
        logger.info("::in getImage::");
        return ResponseEntity.ok(imageService.getImages());
    }

    @GetMapping(value ={"admin/image/{id}/v1", "image/{id}/v1"})
    public ResponseEntity getImageById(@PathVariable @NonNull Long id) {
        logger.info("::in getImageById::");
        return ResponseEntity.ok(imageService.getImageById(id));
    }

    @DeleteMapping("admin/image/{id}/v1")
    public ResponseEntity deleteImageById(@PathVariable @NonNull Long id) {
        logger.info("::in deleteImageById::");
        return ResponseEntity.ok(imageService.deleteImageById(id));
    }

    @PostMapping("admin/image/v1")
    public ResponseEntity createImage(@RequestBody Image image) {
        logger.info("::in createImage::");
        return ResponseEntity.ok(imageService.createOrUpdateImage(image));
    }

    @PutMapping("admin/image/v1")
    public ResponseEntity updateImage(@RequestBody Image image) {
        logger.info("::in updateImage::");
        return ResponseEntity.ok(imageService.createOrUpdateImage(image));
    }

}
