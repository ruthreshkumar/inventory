package com.display.inventory.controller;

import com.display.inventory.entity.Cart;
import com.display.inventory.entity.Content;
import com.display.inventory.services.CartService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/cart/")
public class CartController {

    private final static Logger logger = LogManager.getLogger(CartController.class);

    @Autowired
    private CartService cartService;

    @PostMapping("create/v1")
    public ResponseEntity createCart(@RequestBody Cart cart) {
        logger.info("::in createCart::");
        return ResponseEntity.ok(cartService.createOrUpdateCart(cart));
    }

    @PutMapping("replace/v1")
    public ResponseEntity replaceContent(@RequestBody Cart cart) {
        logger.info("::in replaceContent::");
        return ResponseEntity.ok(cartService.createOrUpdateCart(cart));
    }

    @GetMapping("{cartId}/v1")
    public ResponseEntity getCart(@PathVariable Long cartId) {
        logger.info("::in getCart::");
        return ResponseEntity.ok(cartService.getCart(cartId));
    }

    @PutMapping("{cartId}/content/v1")
    public ResponseEntity addContentToCart(@PathVariable Long cartId, @RequestBody Content content) {
        logger.info("::in addContentToCart::");
        return ResponseEntity.ok(cartService.addProductToCart(cartId, content));
    }

    @DeleteMapping("{cartId}/content/{contentId}/v1")
    public ResponseEntity deleteContentFromCart(@PathVariable Long cartId, @PathVariable Long contentId) {
        logger.info("::in deleteContentFromCart::");
        return ResponseEntity.ok(cartService.deleteProductFromCart(cartId, contentId));
    }

}
