package com.display.inventory.controller;

import com.display.inventory.security.User;
import com.display.inventory.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @PostMapping(value ={"sign/up"})
    public ResponseEntity createUser(@RequestBody User user) {
        logger.info("::in createUser::");
        return ResponseEntity.ok(userService.saveUser(user, "USER"));
    }

    @PostMapping(value ={"sign/up/client"})
    public ResponseEntity createClientUser(@RequestBody User user) {
        logger.info("::in createClientUser::");
        return ResponseEntity.ok(userService.saveUser(user, "ADMIN"));
    }

    @PutMapping(value ={"sign/edit"})
    public ResponseEntity editUser(@RequestBody User user) {
        logger.info("::in editUser::");
        return ResponseEntity.ok(userService.editUser(user));
    }

    @GetMapping("user/info/{userName}")
    public ResponseEntity getUser(@PathVariable String userName) {
        logger.info("::in getUser::");
        return ResponseEntity.ok(userService.getUserInfo(userName));
    }

}
