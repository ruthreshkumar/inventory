package com.display.inventory.controller;

import com.display.inventory.services.ProductService;
import com.display.inventory.services.SubCategoryService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class BasicRelationController {

    private static final Logger logger = LogManager.getLogger(BasicRelationController.class);

    @Autowired
    private SubCategoryService subCategoryService;
    @Autowired
    private ProductService productService;

    @GetMapping(value ={"admin/category/{catId}/sub_category/v1", "category/{catId}/category/v1"})
    public ResponseEntity getSubCategoryByCategoryId(@PathVariable Long catId) {
        logger.info("::in getSubCategoryByCategoryId::");
        return ResponseEntity.ok(subCategoryService.getSubCategoryByCategoryId(catId));
    }

    @GetMapping(value ={"admin/sub_category/{subCatId}/product/v1", "sub_category/{subCatId}/product/v1"})
    public ResponseEntity getProductsBySubCategoryId(@PathVariable Long subCatId) {
        logger.info("::in getProductsBySubCategoryId::");
        return ResponseEntity.ok(productService.getProductsBySubCategoryId(subCatId));
    }

    @GetMapping(value ={"admin/business/{bizId}/product/v1", "business/{bizId}/product/v1"})
    public ResponseEntity getProductsByBusinessId(@PathVariable Long bizId) {
        logger.info("::in getProductsByBusinessId::");
        return ResponseEntity.ok(productService.getProductsByBusinessId(bizId));
    }

}
