package com.display.inventory.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "sub_category")
@SQLDelete(sql = "UPDATE sub_category SET deleted=true WHERE id=?")
@Where(clause = "deleted = false")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubCategory extends BaseEntity {

    private String description;
    private String offer;
    @Column(name = "category_id")
    private Long categoryId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
}
