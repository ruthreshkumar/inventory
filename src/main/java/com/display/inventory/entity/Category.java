package com.display.inventory.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;

@Entity
@SQLDelete(sql = "UPDATE category SET deleted=true WHERE id=?")
@Where(clause = "deleted = false")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Category extends BaseEntity {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
