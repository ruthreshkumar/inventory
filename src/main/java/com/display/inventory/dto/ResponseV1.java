package com.display.inventory.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "Data", "ResponseMessage", "ResponseCode", "ResponseText", "Exception" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseV1 {

    @JsonProperty("Data")
    private Object data;
    @JsonProperty("ResponseMessage")
    private String responseMessage;
    @JsonProperty("ResponseCode")
    private String responseCode;
    @JsonProperty("ResponseText")
    private String responseText;
    @JsonProperty("Exception")
    private Object exception;

    public ResponseV1(String responseMessage, String responseCode, String responseText) {
        this.responseMessage = responseMessage;
        this.responseCode = responseCode;
        this.responseText = responseText;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public Object getException() {
        return exception;
    }

    public void setException(Object exception) {
        this.exception = exception;
    }
}
