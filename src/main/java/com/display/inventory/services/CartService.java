package com.display.inventory.services;

import com.display.inventory.dto.ResponseV1;
import com.display.inventory.entity.Business;
import com.display.inventory.entity.Cart;
import com.display.inventory.entity.Content;
import com.display.inventory.entity.Image;
import com.display.inventory.repo.CartRepository;
import com.display.inventory.repo.ContentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class CartService {

    private static final Logger logger = LogManager.getLogger(CartService.class);

    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private ContentRepository contentRepository;

    public ResponseV1 createOrUpdateCart(@NonNull Cart entity) {

        ResponseV1 response;
        String exceptionMessage = null;
        String responseCode = "500";
        String responseText = "INTERNAL SERVER ERROR";
        try {
            if (entity.getId() == null) {
                entity.setCreated(LocalDateTime.now());
                entity.setUpdated(LocalDateTime.now());
                Cart cart = cartRepository.save(entity);
                Long cartId = cart.getId();
                if (entity.getContents() != null) {
                    for(Content content : entity.getContents()) {
                        content.setCartId(cartId);
                        content.setCreated(LocalDateTime.now());
                    }
                    contentRepository.saveAll(entity.getContents());
                }
            } else {
                Optional<Cart> cartOp = cartRepository.findById(entity.getId());
                Cart editCart = null;
                if (cartOp.isPresent()) {
                    editCart = cartOp.get();
                    if (entity.getContents() != null) {
                        for (Content content : entity.getContents()) {
                            content.setCartId(editCart.getId());
                            content.setCreated(LocalDateTime.now());
                        }
                        contentRepository.saveAll(entity.getContents());
                    }
                    editCart.setUpdated(LocalDateTime.now());
                    Cart cart = cartRepository.save(editCart);
                    cart.setContents(entity.getContents());
                    entity = cart;
                } else {
                    exceptionMessage = "Cart not found for id: " + entity.getId();
                    responseCode = "404";
                    responseText = "NOT FOUND";
                    throw new Exception();
                }
            }
        } catch(Exception e) {
            logger.info("Error in creating cart "+ e, e);
            response = new ResponseV1("", responseCode, responseText);
            response.setData("{}");
            if(exceptionMessage == null) {
                exceptionMessage = e.getMessage();
            }
            response.setException(Arrays.asList(exceptionMessage));
            return response;
        }
        response = new ResponseV1("", "200", "OK");
        response.setData(entity);
        response.setException("{}");
        return response;
    }

    public ResponseV1 getCart(@NonNull Long cartId) {

        Optional<Cart> cartOp = cartRepository.findById(cartId);
        ResponseV1 response;
        Cart cart = null;
        if (cartOp.isPresent()) {
            cart = cartOp.get();
            List<Content> contents = contentRepository.findByCartId(cartId);
            if(!contents.isEmpty()) {
                cart.setContents(contents);
            }
        } else {
            logger.info("Cart not found for id: "+ cartId);
            response = new ResponseV1("", "404", "NOT FOUND");
            response.setData("{}");
            response.setException(Arrays.asList("Business not found for id: "+ cartId));
            return response;
        }
        response = new ResponseV1("", "200", "OK");
        response.setData(cart);
        response.setException("{}");
        return response;
    }

    public ResponseV1 addProductToCart(Long cartId, @NonNull Content entity) {

        ResponseV1 response;
        String exceptionMessage = null;
        String responseCode = "500";
        String responseText = "INTERNAL SERVER ERROR";
        Cart cart = null;
        try {
            if (cartId == null) {
                exceptionMessage = "Cart id cannot be null";
                responseCode = "400";
                responseText = "BAD REQUEST";
                throw new Exception();
            } else {
                Optional<Cart> cartOp = cartRepository.findById(cartId);
                Cart editCart = null;
                if (cartOp.isPresent()) {
                    editCart = cartOp.get();
                    entity.setCartId(editCart.getId());
                    entity.setCreated(LocalDateTime.now());
                    entity = contentRepository.save(entity);
                    editCart.setUpdated(LocalDateTime.now());
                    cart = cartRepository.save(editCart);
                    List<Content> cartContents = contentRepository.findByCartId(cartId);
                    cart.setContents(cartContents);
                } else {
                    exceptionMessage = "Cart not found for id: " + cartId;
                    responseCode = "404";
                    responseText = "NOT FOUND";
                    throw new Exception();
                }
            }
        } catch(Exception e) {
            logger.info("Error in adding content to cart "+ e, e);
            response = new ResponseV1("", responseCode, responseText);
            response.setData("{}");
            if(exceptionMessage == null) {
                exceptionMessage = e.getMessage();
            }
            response.setException(Arrays.asList(exceptionMessage));
            return response;
        }
        response = new ResponseV1("", "200", "OK");
        response.setData(cart);
        response.setException("{}");
        return response;
    }

    public ResponseV1 deleteProductFromCart(Long cartId, @NonNull Long contentId) {

        ResponseV1 response;
        String exceptionMessage = null;
        String responseCode = "500";
        String responseText = "INTERNAL SERVER ERROR";
        Cart cart = null;
        try {
            if (cartId == null || contentId == null) {
                responseCode = "400";
                exceptionMessage = "Cart id/Content id is null";
                responseText = "BAD REQUEST";
                throw new Exception();
            } else {
                Optional<Cart> cartOp = cartRepository.findById(cartId);
                Cart editCart = null;
                if (cartOp.isPresent()) {
                    editCart = cartOp.get();
                    contentRepository.deleteById(contentId);
                    editCart.setUpdated(LocalDateTime.now());
                    cart = cartRepository.save(editCart);
                    List<Content> cartContents = contentRepository.findByCartId(cartId);
                    cart.setContents(cartContents);
                } else {
                    exceptionMessage = "Cart not found for id: " + cartId;
                    responseCode = "404";
                    responseText = "NOT FOUND";
                    throw new Exception();
                }
            }
        } catch(Exception e) {
            logger.info("Error in deleting content from cart "+ e, e);
            response = new ResponseV1("", responseCode, responseText);
            response.setData("{}");
            if(exceptionMessage == null) {
                exceptionMessage = e.getMessage();
            }
            response.setException(Arrays.asList(exceptionMessage));
            return response;
        }
        response = new ResponseV1("", "200", "OK");
        response.setData(cart);
        response.setException("{}");
        return response;
    }

}
