package com.display.inventory.services;

import com.display.inventory.dto.ResponseV1;
import com.display.inventory.entity.Image;
import com.display.inventory.repo.ImageRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class ImageService {

    private static final Logger logger = LogManager.getLogger(ImageService.class);

    @Autowired
    ImageRepository imageRepository;

    public ResponseV1 getImages() {

        List<Image> images = new ArrayList<>();
        for(Image image : imageRepository.findAll()) {
            images.add(image);
        }
        ResponseV1 response = new ResponseV1("", "200", "OK");
        response.setData(images);
        response.setException("{}");
        return response;
    }

    public ResponseV1 getImageById(Long id) {

        Optional<Image> imageOp = imageRepository.findById(id);
        Image image;
        ResponseV1 response;
        if(imageOp.isPresent()) {
            image = imageOp.get();
            response = new ResponseV1("", "200", "OK");
            response.setData(image);
            response.setException("{}");
        } else {
            logger.info("Image not found for id: "+ id);
            response = new ResponseV1("", "404", "NOT FOUND");
            response.setData("{}");
            response.setException(Arrays.asList("Image not found for id: "+ id));
        }
        return response;
    }

    public ResponseV1 createOrUpdateImage(Image entity) {

        ResponseV1 response;
        String exceptionMessage = null;
        String responseCode = "500";
        String responseText = "INTERNAL SERVER ERROR";
        try {
            if(entity.getId() == null) {
                entity.setCreated(LocalDateTime.now());
                entity.setUpdated(LocalDateTime.now());
                entity = imageRepository.save(entity);
            } else {
                Optional<Image> image = imageRepository.findById(entity.getId());
                Image editImage = null;
                if(image.isPresent()) {
                    editImage = image.get();
                    if(entity.getType() != null)
                        editImage.setType(entity.getType());
                    if(entity.getImages() != null) {
                        String[] newImages = entity.getImages().split(",");
                        StringBuilder currentImages = new StringBuilder(editImage.getImages());
                        for(String img : newImages) {
                            if(!currentImages.toString().contains(img)) {
                                currentImages = currentImages.append(","+ img);
                            }
                        }
                        editImage.setImages(currentImages.toString());
                    }
                    editImage.setUpdated(LocalDateTime.now());
                    editImage = imageRepository.save(editImage);
                    entity = editImage;
                } else {
                    exceptionMessage = "Image not found for id: "+ entity.getId();
                    responseCode = "404";
                    responseText = "NOT FOUND";
                    throw new Exception();
                }
            }
        } catch(Exception e) {
            logger.info("Error in saving/editing image "+ e, e);
            response = new ResponseV1("", responseCode, responseText);
            response.setData("{}");
            if(exceptionMessage == null) {
                exceptionMessage = e.getMessage();
            }
            response.setException(Arrays.asList(exceptionMessage));
            return response;
        }
        response = new ResponseV1("", "200", "OK");
        response.setData(entity);
        response.setException("{}");
        return response;
    }

    //needs remove some images from the comma seperated image urls
    public ResponseV1 deleteImageById(Long id) {

        Optional<Image> image = imageRepository.findById(id);
        if(image.isPresent()) {
            imageRepository.deleteById(id);
        }
        ResponseV1 response = new ResponseV1("", "200", "OK");
        response.setData("{}");
        response.setException("{}");
        return response;
    }

}
