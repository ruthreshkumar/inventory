package com.display.inventory.services;

import com.display.inventory.dto.ResponseV1;
import com.display.inventory.entity.SubCategory;
import com.display.inventory.repo.SubCategoryRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class SubCategoryService {

    private static final Logger logger = LogManager.getLogger(SubCategoryService.class);

    @Autowired
    SubCategoryRepository subCategoryRepository;

    public ResponseV1 getSubCategory() {

        List<SubCategory> subCategories = new ArrayList<>();
        for(SubCategory subCategory : subCategoryRepository.findAll()) {
            subCategory.setName(subCategory.getDisplayName());
            subCategory.setDisplayName(null);
            subCategories.add(subCategory);
        }
        ResponseV1 response = new ResponseV1("", "200", "OK");
        response.setData(subCategories);
        response.setException("{}");
        return response;
    }

    public ResponseV1 getSubCategoryById(Long id) {

        Optional<SubCategory> subCategoryOp = subCategoryRepository.findById(id);
        SubCategory subCategory;
        ResponseV1 response;
        if(subCategoryOp.isPresent()) {
            subCategory = subCategoryOp.get();
            subCategory.setName(subCategory.getDisplayName());
            subCategory.setDisplayName(null);
            response = new ResponseV1("", "200", "OK");
            response.setData(subCategory);
            response.setException("{}");
        } else {
            logger.info("SubCategory not found for id: "+ id);
            response = new ResponseV1("", "404", "NOT FOUND");
            response.setData("{}");
            response.setException(Arrays.asList("SubCategory not found for id: "+ id));
        }
        return response;
    }

    public ResponseV1 createOrUpdateSubCategory(SubCategory entity) {

        ResponseV1 response;
        String exceptionMessage = null;
        String responseCode = "500";
        String responseText = "INTERNAL SERVER ERROR";
        try {
            if(entity.getId() == null) {
                entity.setDisplayName(entity.getName());
                entity.setCreated(LocalDateTime.now());
                entity.setUpdated(LocalDateTime.now());
                entity = subCategoryRepository.save(entity);
            } else {
                Optional<SubCategory> subCategory = subCategoryRepository.findById(entity.getId());
                SubCategory editSubCategory = null;
                if(subCategory.isPresent()) {
                    editSubCategory = subCategory.get();
                    if(entity.getName() != null)
                        editSubCategory.setDisplayName(entity.getName());
                    if(entity.getDescription() != null)
                        editSubCategory.setDescription(entity.getDescription());
                    if(entity.getOffer() != null)
                        editSubCategory.setOffer(entity.getOffer());
                    if(entity.getCategoryId() != null)
                        editSubCategory.setCategoryId(entity.getCategoryId());
                    editSubCategory.setUpdated(LocalDateTime.now());
                    editSubCategory = subCategoryRepository.save(editSubCategory);
                    entity = editSubCategory;
                } else {
                    exceptionMessage = "SubCategory not found for id: "+ entity.getId();
                    responseCode = "404";
                    responseText = "NOT FOUND";
                    throw new Exception();
                }
            }
        } catch(Exception e) {
            logger.info("Error in saving/editing sub Category "+ e, e);
            response = new ResponseV1("", responseCode, responseText);
            response.setData("{}");
            if(exceptionMessage == null) {
                exceptionMessage = e.getMessage();
            }
            response.setException(Arrays.asList(exceptionMessage));
            return response;
        }
        entity.setName(entity.getDisplayName());
        entity.setDisplayName(null);
        response = new ResponseV1("", "200", "OK");
        response.setData(entity);
        response.setException("{}");
        return response;
    }

    public ResponseV1 deleteSubCategoryById(Long id) {

        Optional<SubCategory> subCategory = subCategoryRepository.findById(id);
        if(subCategory.isPresent()) {
            subCategoryRepository.deleteById(id);
        }
        ResponseV1 response = new ResponseV1("", "200", "OK");
        response.setData("{}");
        response.setException("{}");
        return response;
    }

    public ResponseV1 getSubCategoryByCategoryId(Long categoryId) {

        List<SubCategory> subCategoryOp = subCategoryRepository.findByCategoryId(categoryId);
        ResponseV1 response;
        if(!subCategoryOp.isEmpty()) {
            List<SubCategory> subCategories = new ArrayList<>();
            for(SubCategory subCategory : subCategoryOp) {
                subCategory.setName(subCategory.getDisplayName());
                subCategory.setDisplayName(null);
                subCategories.add(subCategory);
            }
            response = new ResponseV1("", "200", "OK");
            response.setData(subCategories);
            response.setException("{}");
        } else {
            logger.info("SubCategory not found for category id: "+ categoryId);
            response = new ResponseV1("", "404", "NOT FOUND");
            response.setData("{}");
            response.setException(Arrays.asList("SubCategory not found for category id: "+ categoryId));
        }
        return response;
    }

}
