package com.display.inventory.services;

import com.display.inventory.dto.ResponseV1;
import com.display.inventory.entity.Category;
import com.display.inventory.repo.CategoryRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    private static final Logger logger = LogManager.getLogger(CategoryService.class);

    @Autowired
    CategoryRepository categoryRepository;

    public ResponseV1 getCategory() {

        List<Category> categories = new ArrayList<>();
        for(Category category : categoryRepository.findAll()) {
            category.setName(category.getDisplayName());
            category.setDisplayName(null);
            categories.add(category);
        }
        ResponseV1 response = new ResponseV1("", "200", "OK");
        response.setData(categories);
        response.setException("{}");
        return response;
    }

    public ResponseV1 getCategoryById(Long id) {

        Optional<Category> categoryOp = categoryRepository.findById(id);
        Category category;
        ResponseV1 response;
        if(categoryOp.isPresent()) {
            category = categoryOp.get();
            category.setName(category.getDisplayName());
            category.setDisplayName(null);
            response = new ResponseV1("", "200", "OK");
            response.setData(category);
            response.setException("{}");
        } else {
            logger.info("Category not found for id: "+ id);
            response = new ResponseV1("", "404", "NOT FOUND");
            response.setData("{}");
            response.setException(Arrays.asList("Category not found for id: "+ id));
        }
        return response;
    }

    public ResponseV1 createOrUpdateCategory(Category entity) {

        ResponseV1 response;
        String exceptionMessage = null;
        String responseCode = "500";
        String responseText = "INTERNAL SERVER ERROR";
        try {
            if(entity.getId() == null) {
                entity.setDisplayName(entity.getName());
                entity.setCreated(LocalDateTime.now());
                entity.setUpdated(LocalDateTime.now());
                entity = categoryRepository.save(entity);
            } else {
                Optional<Category> category = categoryRepository.findById(entity.getId());
                Category editCategory = null;
                if(category.isPresent()) {
                    editCategory = category.get();
                    if(entity.getName() != null)
                        editCategory.setDisplayName(entity.getName());
                    if(entity.getDescription() != null)
                        editCategory.setDescription(entity.getDescription());
                    editCategory.setUpdated(LocalDateTime.now());
                    editCategory = categoryRepository.save(editCategory);
                    entity = editCategory;
                } else {
                    exceptionMessage = "Category not found for id: "+ entity.getId();
                    responseCode = "404";
                    responseText = "NOT FOUND";
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.info("Error in saving/editing category "+ e, e);
            response = new ResponseV1("", responseCode, responseText);
            response.setData("{}");
            if(exceptionMessage == null) {
                exceptionMessage = e.getMessage();
            }
            response.setException(Arrays.asList(exceptionMessage));
            return response;
        }
        entity.setName(entity.getDisplayName());
        entity.setDisplayName(null);
        response = new ResponseV1("", "200", "OK");
        response.setData(entity);
        response.setException("{}");
        return response;
    }

    public ResponseV1 deleteCategoryById(Long id) {

        Optional<Category> category = categoryRepository.findById(id);
        if(category.isPresent()) {
            categoryRepository.deleteById(id);
        }
        ResponseV1 response = new ResponseV1("", "200", "OK");
        response.setData("{}");
        response.setException("{}");
        return response;
    }

}
