package com.display.inventory.services;

import com.display.inventory.dto.ResponseV1;
import com.display.inventory.entity.Image;
import com.display.inventory.entity.Product;
import com.display.inventory.repo.ImageRepository;
import com.display.inventory.repo.ProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private static final Logger logger = LogManager.getLogger(ProductService.class);

    @Autowired
    ProductRepository productRepository;
    @Autowired
    ImageRepository imageRepository;

    public ResponseV1 getProducts() {

        List<Product> products = new ArrayList<>();
        for(Product product : productRepository.findAll()) {
            product.setName(product.getDisplayName());
            product.setDisplayName(null);
            if(product.getImage() != null) {
                product.setImages(Arrays.asList(product.getImage().getImages().split(",")));
                product.setImagesId(product.getImage().getId());
                product.setImage(null);
            }
            products.add(product);
        }
        ResponseV1 response = new ResponseV1("", "200", "OK");
        response.setData(products);
        response.setException("{}");
        return response;
    }

    public ResponseV1 getProductById(Long id) {

        Optional<Product> productOp = productRepository.findById(id);
        Product product;
        ResponseV1 response;
        if(productOp.isPresent()) {
            product = productOp.get();
            product.setName(product.getDisplayName());
            product.setDisplayName(null);
            if(product.getImage() != null) {
                product.setImages(Arrays.asList(product.getImage().getImages().split(",")));
                product.setImagesId(product.getImage().getId());
                product.setImage(null);
            }
            response = new ResponseV1("", "200", "OK");
            response.setData(product);
            response.setException("{}");
        } else {
            logger.info("Product not found for id: "+ id);
            response = new ResponseV1("", "404", "NOT FOUND");
            response.setData("{}");
            response.setException(Arrays.asList("Product not found for id: "+ id));
        }
        return response;
    }

    public ResponseV1 createOrUpdateProduct(Product entity) {

        ResponseV1 response;
        String exceptionMessage = null;
        String responseCode = "500";
        String responseText = "INTERNAL SERVER ERROR";
        try {
            if(entity.getId() == null) {
                entity.setDisplayName(entity.getName());
                entity.setCreated(LocalDateTime.now());
                entity.setUpdated(LocalDateTime.now());
                if(entity.getImagesId() != null) {
                    Optional<Image> imageOp = imageRepository.findById(entity.getImagesId());
                    if(imageOp.isPresent()) {
                        entity.setImage(imageOp.get());
                    }
                }
                entity = productRepository.save(entity);
            } else {
                Optional<Product> product = productRepository.findById(entity.getId());
                Product editProduct = null;
                if(product.isPresent()) {
                    editProduct = product.get();
                    if(entity.getName() != null)
                        editProduct.setDisplayName(entity.getName());
                    if(entity.getDescription() != null)
                        editProduct.setDescription(entity.getDescription());
                    if(entity.getImagesId() != null) {
                        Optional<Image> imageOp = imageRepository.findById(entity.getImagesId());
                        if(imageOp.isPresent()) {
                            editProduct.setImage(imageOp.get());
                        }
                    }
                    if(entity.getCost() != null)
                        editProduct.setCost(entity.getCost());
                    if(entity.getDiscount() != null)
                        editProduct.setDiscount(entity.getDiscount());
                    if(entity.getTax() != null)
                        editProduct.setTax(entity.getTax());
                    if(entity.getPrice() != null)
                        editProduct.setPrice(entity.getPrice());
                    if(entity.getSubCategoryId() != null)
                        editProduct.setSubCategoryId(entity.getSubCategoryId());
                    if(entity.getBusinessId() != null)
                        editProduct.setBusinessId(entity.getBusinessId());
                    editProduct.setUpdated(LocalDateTime.now());
                    editProduct = productRepository.save(editProduct);
                    entity = editProduct;
                } else {
                    exceptionMessage = "Product not found for id: "+ entity.getId();
                    responseCode = "404";
                    responseText = "NOT FOUND";
                    throw new Exception();
                }
            }
        } catch(Exception e) {
            logger.info("Error in saving/editing product "+ e, e);
            response = new ResponseV1("", responseCode, responseText);
            response.setData("{}");
            if(exceptionMessage == null) {
                exceptionMessage = e.getMessage();
            }
            response.setException(Arrays.asList(exceptionMessage));
            return response;
        }
        entity.setName(entity.getDisplayName());
        entity.setDisplayName(null);
        if(entity.getImage() != null) {
            entity.setImages(Arrays.asList(entity.getImage().getImages().split(",")));
            entity.setImagesId(entity.getImage().getId());
            entity.setImage(null);
        }
        response = new ResponseV1("", "200", "OK");
        response.setData(entity);
        response.setException("{}");
        return response;
    }

    public ResponseV1 deleteProductById(Long id) {

        Optional<Product> product = productRepository.findById(id);
        if(product.isPresent()) {
            productRepository.deleteById(id);
        }
        ResponseV1 response = new ResponseV1("", "200", "OK");
        response.setData("{}");
        response.setException("{}");
        return response;
    }

    public ResponseV1 getProductsBySubCategoryId(Long subCategoryId) {

        List<Product> productOp = productRepository.findBySubCategoryId(subCategoryId);
        ResponseV1 response;
        if(!productOp.isEmpty()) {
            List<Product> products = new ArrayList<>();
            for(Product product : productOp) {
                product.setName(product.getDisplayName());
                product.setDisplayName(null);
                if(product.getImage() != null) {
                    product.setImages(Arrays.asList(product.getImage().getImages().split(",")));
                    product.setImagesId(product.getImage().getId());
                    product.setImage(null);
                }
                products.add(product);
            }
            response = new ResponseV1("", "200", "OK");
            response.setData(products);
            response.setException("{}");
        } else {
            logger.info("Product not found for sub category id: "+ subCategoryId);
            response = new ResponseV1("", "404", "NOT FOUND");
            response.setData("{}");
            response.setException(Arrays.asList("Product not found for sub category id: "+ subCategoryId));
        }
        return response;
    }

    public ResponseV1 getProductsByBusinessId(Long businessId) {

        List<Product> productOp = productRepository.findByBusinessId(businessId);
        ResponseV1 response;
        if(!productOp.isEmpty()) {
            List<Product> products = new ArrayList<>();
            for(Product product : productOp) {
                product.setName(product.getDisplayName());
                product.setDisplayName(null);
                if(product.getImage() != null) {
                    product.setImages(Arrays.asList(product.getImage().getImages().split(",")));
                    product.setImagesId(product.getImage().getId());
                    product.setImage(null);
                }
                products.add(product);
            }
            response = new ResponseV1("", "200", "OK");
            response.setData(products);
            response.setException("{}");
        } else {
            logger.info("Product not found for business id: "+ businessId);
            response = new ResponseV1("", "404", "NOT FOUND");
            response.setData("{}");
            response.setException(Arrays.asList("Product not found for business id: "+ businessId));
        }
        return response;
    }

}
