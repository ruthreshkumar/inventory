package com.display.inventory.services;

import com.display.inventory.dto.ResponseV1;
import com.display.inventory.entity.Business;
import com.display.inventory.entity.Image;
import com.display.inventory.repo.BusinessRepository;
import com.display.inventory.repo.ImageRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class BusinessService {

    private static final Logger logger = LogManager.getLogger(BusinessService.class);

    @Autowired
    BusinessRepository businessRepository;
    @Autowired
    ImageRepository imageRepository;

    public ResponseV1 getBusiness() {

        List<Business> businesses = new ArrayList<>();
        for(Business business : businessRepository.findAll()) {
            business.setName(business.getDisplayName());
            business.setDisplayName(null);
            if(business.getImage() != null) {
                business.setImages(Arrays.asList(business.getImage().getImages().split(",")));
                business.setImagesId(business.getImage().getId());
                business.setImage(null);
            }
            businesses.add(business);
        }
        ResponseV1 response = new ResponseV1("", "200", "OK");
        response.setData(businesses);
        response.setException("{}");
        return response;
    }

    public ResponseV1 getBusinessById(Long id) {

        Optional<Business> businessOp = businessRepository.findById(id);
        Business business;
        ResponseV1 response;
        if(businessOp.isPresent()) {
            business = businessOp.get();
            business.setName(business.getDisplayName());
            business.setDisplayName(null);
            if(business.getImage() != null) {
                business.setImages(Arrays.asList(business.getImage().getImages().split(",")));
                business.setImagesId(business.getImage().getId());
                business.setImage(null);
            }
            response = new ResponseV1("", "200", "OK");
            response.setData(business);
            response.setException("{}");
        } else {
            logger.info("Business not found for id: "+ id);
            response = new ResponseV1("", "404", "NOT FOUND");
            response.setData("{}");
            response.setException(Arrays.asList("Business not found for id: "+ id));
        }
        return response;
    }

    public ResponseV1 createOrUpdateBusiness(@NonNull Business entity) {

        ResponseV1 response;
        String exceptionMessage = null;
        String responseCode = "500";
        String responseText = "INTERNAL SERVER ERROR";
        try {
            if (entity.getId() == null) {
                if (entity.getType() == null)
                    entity.setType("general");
                entity.setDisplayName(entity.getName());
                entity.setCreated(LocalDateTime.now());
                entity.setUpdated(LocalDateTime.now());
                if (entity.getImagesId() != null) {
                    Optional<Image> imageOp = imageRepository.findById(entity.getImagesId());
                    if (imageOp.isPresent()) {
                        entity.setImage(imageOp.get());
                    }
                }
                entity = businessRepository.save(entity);
            } else {
                Optional<Business> business = businessRepository.findById(entity.getId());
                Business editBusiness = null;
                if (business.isPresent()) {
                    editBusiness = business.get();
                    if (entity.getName() != null)
                        editBusiness.setDisplayName(entity.getName());
                    if (entity.getEmail() != null)
                        editBusiness.setEmail(entity.getEmail());
                    if (entity.getAddress() != null)
                        editBusiness.setAddress(entity.getAddress());
                    if (entity.getDescription() != null)
                        editBusiness.setDescription(entity.getDescription());
                    if (entity.getPhone() != null)
                        editBusiness.setPhone(entity.getPhone());
                    if (entity.getType() != null)
                        editBusiness.setType(entity.getType());
                    if (entity.getImageIcon() != null)
                        editBusiness.setImageIcon(entity.getImageIcon());
                    if (entity.getImagesId() != null) {
                        Optional<Image> imageOp = imageRepository.findById(entity.getImagesId());
                        if (imageOp.isPresent()) {
                            editBusiness.setImage(imageOp.get());
                        }
                    }
                    editBusiness.setUpdated(LocalDateTime.now());
                    editBusiness = businessRepository.save(editBusiness);
                    entity = editBusiness;
                } else {
                    exceptionMessage = "Business not found for id: " + entity.getId();
                    responseCode = "404";
                    responseText = "NOT FOUND";
                    throw new Exception();
                }
            }
        } catch(Exception e) {
            logger.info("Error in saving/editing business "+ e, e);
            response = new ResponseV1("", responseCode, responseText);
            response.setData("{}");
            if(exceptionMessage == null) {
                exceptionMessage = e.getMessage();
            }
            response.setException(Arrays.asList(exceptionMessage));
            return response;
        }
        entity.setName(entity.getDisplayName());
        entity.setDisplayName(null);
        if(entity.getImage() != null) {
            entity.setImages(Arrays.asList(entity.getImage().getImages().split(",")));
            entity.setImagesId(entity.getImage().getId());
            entity.setImage(null);
        }
        response = new ResponseV1("", "200", "OK");
        response.setData(entity);
        response.setException("{}");
        return response;
    }

    public ResponseV1 deleteBusinessById(Long id) {

        Optional<Business> business = businessRepository.findById(id);
        if(business.isPresent()) {
            businessRepository.deleteById(id);
        }
        ResponseV1 response = new ResponseV1("", "200", "OK");
        response.setData("{}");
        response.setException("{}");
        return response;
    }

}
