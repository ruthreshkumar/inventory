package com.display.inventory.services;

import com.display.inventory.dto.ResponseV1;
import com.display.inventory.security.User;
import com.display.inventory.security.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

@Service
public class UserService {

    private static final Logger logger = LogManager.getLogger(UserService.class);

    @Autowired
    UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public ResponseV1 saveUser(User user, String userType) {

        ResponseV1 response;
        User entity = null;
        try {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user.setRoles("ROLE_"+ userType);
            user.setActive(true);
            user.setCreated(LocalDateTime.now());
            user.setUpdated(LocalDateTime.now());
            entity = userRepository.save(user);
            entity.setPassword(null);
        } catch (Exception e) {
            logger.info("Error in saving user "+ e, e);
            response = new ResponseV1("", "500", "INTERNAL SERVER ERROR");
            response.setData("{}");
            response.setException(Arrays.asList(e.getMessage()));
            return response;
        }
        response = new ResponseV1("", "200", "OK");
        response.setData(entity);
        response.setException("{}");
        return response;
    }

    public ResponseV1 editUser(User user) {

        ResponseV1 response;
        String exceptionMessage = null;
        String responseCode = "500";
        String responseText = "INTERNAL SERVER ERROR";
        User entity = user;
        try {
            Optional<User> userOp = userRepository.findByUserName(user.getUserName());
            User editUser = null;
            if(userOp.isPresent()) {
                editUser = userOp.get();
                if(entity.getName() != null)
                    editUser.setName(entity.getName());
                if(entity.getEmail() != null)
                    editUser.setEmail(entity.getEmail());
                if(entity.getPassword() != null) {
                    String encodedPass = bCryptPasswordEncoder.encode(entity.getPassword());
                    logger.info("Password changed for user "+ user.getUserName());
                    editUser.setPassword(encodedPass);
                }
                editUser.setUpdated(LocalDateTime.now());
                editUser = userRepository.save(editUser);
                editUser.setPassword(null);
                entity = editUser;
            } else {
                exceptionMessage = "User not found for userName: "+ entity.getName();
                responseCode = "404";
                responseText = "NOT FOUND";
                throw new Exception();
            }
        } catch (Exception e) {
            logger.info("Error in editing user info "+ e, e);
            response = new ResponseV1("", responseCode, responseText);
            response.setData("{}");
            if(exceptionMessage == null) {
                exceptionMessage = e.getMessage();
            }
            response.setException(Arrays.asList(exceptionMessage));
            return response;
        }
        response = new ResponseV1("", "200", "OK");
        response.setData(entity);
        response.setException("{}");
        return response;
    }

    public ResponseV1 getUserInfo(String userName) {

        ResponseV1 response;
        Optional<User> userOp = userRepository.findByUserName(userName);
        User entity = null;
        if(userOp.isPresent()) {
            entity = userOp.get();
            entity.setPassword(null);
        } else {
            logger.info("User not found for userName: "+ userName);
            response = new ResponseV1("", "404", "NOT FOUND");
            response.setData("{}");
            response.setException("User not found for userName: "+ userName);
            return response;
        }
        response = new ResponseV1("", "200", "OK");
        response.setData(entity);
        response.setException("{}");
        return response;
    }

}
